using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace agilia_api.Entities
{
     public class TokenRequest
     {
          public string username { get; set; }
          public string password { get; set; }
          public string grant_type { get; set; }
          public string client_id { get; set; }
          public string client_secret { get; set; }
          public string isProfileLogin { get; set; }
          public string loginCode { get; set; }
          public string resendLastCode { get; set; }
          public string loginCookieCode {get; set;}
     }
}