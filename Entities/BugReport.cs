using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace agilia_api.Entities
{
     public class BugReport
     {
          [Key]
          [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
          public int Id { get; set; }
          [Required]
          [MaxLength(100)]
          public string Email { get; set; }
          [Required]
          public string Title { get; set; }
          [Required]
          public string Description { get; set; }
     }
}