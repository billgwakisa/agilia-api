using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using agilia_api.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace agilia_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("SiteCorsPolicy")]
    public class LoginController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public async Task<string> Post([FromBody] TokenRequest tokenRequest)
        {

            // return "Login controller";

            String url = "https://test02mc.agilia.at/Ebit.Agilia/agiliaapi/token";
            using (var client = new WebClient())
            {
                var data = new {username = "username", password = "password", grant_type = "password", loginCookieCode = "code", client_secret = "test", isProfileLogin = "true"};//new NameValueCollection();
                // data["username"] = "agilia@ebit.at";//tokenRequest.username;
                // data["password"] = "A3dJvvgbe";//tokenRequest.password;
                // data["grant_type"] = "password";
                // data["loginCookieCode"] = "UnuTestProject";
                // data["client_secret"] = "test";
                // data["isProfileLogin"] = "true";
                //data["loginCode"] = "";
                // data["resendLastCode"] = "true";
                //data["loginCookieCode"] = "";

                var dataString = JsonConvert.SerializeObject(data);
                 
                var dict = new Dictionary<string, string>();
                dict.Add("username", tokenRequest.username);//"agilia@ebit.at");
                dict.Add("password", tokenRequest.password); //"A3dJvvgbe");
                dict.Add("grant_type", "password");
                dict.Add("client_id", "UnuTestProject");
                dict.Add("client_secret", "test");
                dict.Add("isProfileLogin", "true");

                var httpClient = new HttpClient();
                var req = new HttpRequestMessage(HttpMethod.Post, url) { Content = new FormUrlEncodedContent(dict) };
                var res = await httpClient.SendAsync(req);
                var responseContent = await res.Content.ReadAsStringAsync();
                // Console.Write(data2);
                // //client.Headers.Add("Accept","application/json");
                // client.Headers.Add(HttpRequestHeader.ContentType, "application/x-www-form-urlencoded");
                // var response = client.UploadString(url, "POST", data2);
                //string responseInString = Encoding.UTF8.GetString(response);

                Console.Write(responseContent);
                return responseContent;
            }
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
