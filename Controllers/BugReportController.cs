using Microsoft.AspNetCore.Mvc;
using agilia_api.Services;
using agilia_api.Entities;
using Microsoft.AspNetCore.Cors;

namespace agilia_api.Controllers
{
     [Route("api/[controller]")]
     [ApiController]
     [EnableCors("SiteCorsPolicy")]
     public class BugReportsController : Controller
     {
          private AgiliaDbContext _context;
          public BugReportsController(AgiliaDbContext context)
          {
               _context = context;
          }
          public IActionResult GetBugReports()
          {
               return Ok(_context.BugReports);
          }

          // GET api/values/5
        [HttpGet("{id}", Name = "GetBugReport")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public IActionResult Create(BugReport bugReport)
        {
        _context.BugReports.Add(bugReport);
        _context.SaveChanges();

            return CreatedAtRoute(routeName: "GetBugReport", 
            routeValues: new {id = bugReport.Id}, 
            value: bugReport);
        }

        //   public string AddBugReports(Employee Emp)  
        // {  
        //     connection();  
        //     com = new SqlCommand("InsertData", con);  
        //     com.CommandType = CommandType.StoredProcedure;  
        //     com.Parameters.AddWithValue("@FName", Emp.FirstName);  
        //     com.Parameters.AddWithValue("@Lname", Emp.LastName);  
        //     com.Parameters.AddWithValue("@Compnay", Emp.Company);  
        //     con.Open();  
        //     int i = com.ExecuteNonQuery();  
        //     con.Close();  
        //     if (i >= 1)  
        //     {  
        //         return "New Employee Added Successfully";  
  
        //     }  
        //     else  
        //     {  
        //         return "Employee Not Added";  
  
        //     }  
        // }  
     }
}