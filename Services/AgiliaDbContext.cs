using Microsoft.EntityFrameworkCore;
using agilia_api.Entities;
namespace agilia_api.Services
{
     public class AgiliaDbContext : DbContext
     {
          public DbSet<BugReport> BugReports { get; set; }
          public AgiliaDbContext(
               DbContextOptions<AgiliaDbContext> options)
               : base(options)
          {
               Database.EnsureCreated();
          }
     }
}