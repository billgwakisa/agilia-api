using System.Collections.Generic;
using System.Linq;
using agilia_api.Entities;
namespace agilia_api.Services
{
     public static class AgiliaDbContextExtensions
     {
          public static void CreateSeedData
               (this AgiliaDbContext context)
          {
               if (context.BugReports.Any())
                    return;
               var bugReport = new List<BugReport>()
               {
                    new BugReport()
                    {
                         Email = "sjdvbosvnd",
                         Title = "This is a title",
                         Description = "Dome desc"
                    },
                    new BugReport()
                    {
                         Email = "Yeah yeah email",
                         Title = "TGit a tutke",
                         Description = "Ddsdc"
                    },
                    new BugReport()
                    {
                         Email = "Eeeeeemail.com",
                         Title = "TTiiiiiele",
                         Description = "Ddfds"
                    }
               };
               context.AddRange(bugReport);
               context.SaveChanges();
          }
     }
}